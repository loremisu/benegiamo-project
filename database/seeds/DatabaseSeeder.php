<?php

use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersSeeder::class);

        $user = new User();

        $user->name = "admin";
        $user->email = 'info@goodjobs.com';
        $user->password = bcrypt('password');

        $user->save();
    }
}
